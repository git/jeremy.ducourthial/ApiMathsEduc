/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Sae301.ApiMathsEduc.lobby;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

/**
 *
 * @author Utilisateur
 */
@RestController
@RequestMapping("/api/lobbies")
public class LobbyController {
    private final LobbyService lobbyService;
    Logger logger = LoggerFactory.getLogger(LobbyController.class); 
    String idAppelTable = "04"; // Table Lobbies

    @Autowired
    public LobbyController(LobbyService lobbyService) {
        this.lobbyService = lobbyService;
    }

    @GetMapping("/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<List<EntityModel<Lobby>>> getAllLobbies() {
        MDC.put("idAppel", idAppelTable + "0");
        logger.info("Getting all lobbies");

        List<Lobby> lobbies = lobbyService.getAllLobbies();
        List<EntityModel<Lobby>> lobbyModels = new ArrayList<>();
        for (Lobby lobby : lobbies) {
            EntityModel<Lobby> model = EntityModel.of(lobby);
            Link selfLink = linkTo(methodOn(LobbyController.class).getLobbyById(lobby.getId())).withSelfRel();
            model.add(selfLink);
            lobbyModels.add(model);
        }
        return ResponseEntity.ok(lobbyModels);
    }

    @GetMapping("/{id}/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<EntityModel<Lobby>> getLobbyById(@PathVariable Integer id) {
        MDC.put("idAppel", idAppelTable + "0" + id);
        logger.info("Getting lobby by ID: {}", id);

        Optional<Lobby> optionalLobbies = lobbyService.getLobbyById(id);
        if (optionalLobbies.isPresent()) {
            Lobby lobby = optionalLobbies.get();

            EntityModel<Lobby> model = EntityModel.of(lobby);

            Link selfLink = linkTo(methodOn(LobbyController.class).getLobbyById(id)).withSelfRel();
            model.add(selfLink);

            Link allLobbiesLink = linkTo(methodOn(LobbyController.class).getAllLobbies()).withRel("allLobbies");
            model.add(allLobbiesLink);

            return ResponseEntity.ok(model);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<Lobby> addLobby(@RequestBody Lobby lobby) {
        MDC.put("idAppel", idAppelTable + "1");
        logger.info("Adding lobby: {}", lobby);
        Lobby addedLobby = lobbyService.addLobby(lobby);
        return new ResponseEntity<>(addedLobby, HttpStatus.CREATED);
    }

    @PutMapping("/{id}/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<Lobby> updateLobby(@PathVariable Integer id, @RequestBody Lobby lobby) {
        MDC.put("idAppel", idAppelTable + "2" + id);
        logger.info("Updating lobby with ID {}: {}", id, lobby);
        Lobby updatedLobby = lobbyService.updateLobby(id, lobby);
        return new ResponseEntity<>(updatedLobby, HttpStatus.OK);
    }

    @DeleteMapping("/{id}/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<Void> deleteLobby(@PathVariable Integer id) {
        MDC.put("idAppel", idAppelTable + "3" + id);
        logger.info("Deleting lobby with ID: {}", id);
        lobbyService.deleteLobby(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}