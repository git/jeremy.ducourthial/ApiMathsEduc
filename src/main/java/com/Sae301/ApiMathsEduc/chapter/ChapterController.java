/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Sae301.ApiMathsEduc.chapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

/**
 *
 * @author Utilisateur
 */
@RestController
@RequestMapping("/api/chapters")
public class ChapterController {
    private final ChapterService chapterService;
    Logger logger = LoggerFactory.getLogger(ChapterController.class); 
    String idAppelTable = "01"; // Table Chapters

    @Autowired
    public ChapterController(ChapterService chapterService) {
        this.chapterService = chapterService;
    }

    @GetMapping("/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<List<EntityModel<Chapter>>> getAllChapters() {
        MDC.put("idAppel", idAppelTable + "0");
        logger.info("Getting all chapters");

        List<Chapter> chapters = chapterService.getAllChapters();
        List<EntityModel<Chapter>> chapterModels = new ArrayList<>();
        for (Chapter chapter : chapters) {
            EntityModel<Chapter> model = EntityModel.of(chapter);
            Link selfLink = linkTo(methodOn(ChapterController.class).getChapterById(chapter.getId())).withSelfRel();
            model.add(selfLink);
            chapterModels.add(model);
        }
        return ResponseEntity.ok(chapterModels);
    }

    @GetMapping("/{id}/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<EntityModel<Chapter>> getChapterById(@PathVariable Integer id) {
        MDC.put("idAppel", idAppelTable + "0" + id);
        logger.info("Getting chapter by ID: {}", id);

        Optional<Chapter> optionalChapter = chapterService.getChapterById(id);
        if (optionalChapter.isPresent()) {
            Chapter chapter = optionalChapter.get();

            EntityModel<Chapter> model = EntityModel.of(chapter);

            Link selfLink = linkTo(methodOn(ChapterController.class).getChapterById(id)).withSelfRel();
            model.add(selfLink);

            Link allChaptersLink = linkTo(methodOn(ChapterController.class).getAllChapters()).withRel("allChapters");
            model.add(allChaptersLink);

            return ResponseEntity.ok(model);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<Chapter> addChapter(@RequestBody Chapter chapter) {
        MDC.put("idAppel", idAppelTable + "1");
        logger.info("Adding chapter: {}", chapter);
        Chapter addedChapter = chapterService.addChapter(chapter);
        return new ResponseEntity<>(addedChapter, HttpStatus.CREATED);
    }

    @PutMapping("/{id}/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<Chapter> updateChapter(@PathVariable Integer id, @RequestBody Chapter chapter) {
        MDC.put("idAppel", idAppelTable + "2" + id);
        logger.info("Updating chapter with ID {}: {}", id, chapter);
        Chapter updatedChapter = chapterService.updateChapter(id, chapter);
        return new ResponseEntity<>(updatedChapter, HttpStatus.OK);
    }

    @DeleteMapping("/{id}/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<Void> deleteChapter(@PathVariable Integer id) {
        MDC.put("idAppel", idAppelTable + "3" + id);
        logger.info("Deleting chapter with ID: {}", id);
        chapterService.deleteChapter(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}