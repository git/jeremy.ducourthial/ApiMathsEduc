/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Sae301.ApiMathsEduc.player;

import com.Sae301.ApiMathsEduc.question.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

/**
 *
 * @author Utilisateur
 */
@RestController
@RequestMapping("/api/players")
public class PlayerController {
    private final PlayerService questionService;
    Logger logger = LoggerFactory.getLogger(PlayerController.class); 
    String idAppelTable = "05"; // Table Players

    @Autowired
    public PlayerController(PlayerService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<List<EntityModel<Player>>> getAllPlayers() {
        MDC.put("idAppel", idAppelTable + "0");
        logger.info("Getting all questions");

        List<Player> questions = questionService.getAllPlayers();
        List<EntityModel<Player>> questionModels = new ArrayList<>();
        for (Player question : questions) {
            EntityModel<Player> model = EntityModel.of(question);
            Link selfLink = linkTo(methodOn(PlayerController.class).getPlayerById(question.getId())).withSelfRel();
            model.add(selfLink);
            questionModels.add(model);
        }
        return ResponseEntity.ok(questionModels);
    }

    @GetMapping("/{id}/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<EntityModel<Player>> getPlayerById(@PathVariable Integer id) {
        MDC.put("idAppel", idAppelTable + "0" + id);
        logger.info("Getting question by ID: {}", id);

        Optional<Player> optionalPlayers = questionService.getPlayerById(id);
        if (optionalPlayers.isPresent()) {
            Player question = optionalPlayers.get();

            EntityModel<Player> model = EntityModel.of(question);

            Link selfLink = linkTo(methodOn(PlayerController.class).getPlayerById(id)).withSelfRel();
            model.add(selfLink);

            Link allPlayersLink = linkTo(methodOn(PlayerController.class).getAllPlayers()).withRel("allPlayers");
            model.add(allPlayersLink);

            return ResponseEntity.ok(model);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<Player> addPlayer(@RequestBody Player question) {
        MDC.put("idAppel", idAppelTable + "1");
        logger.info("Adding question: {}", question);
        Player addedPlayer = questionService.addPlayer(question);
        return new ResponseEntity<>(addedPlayer, HttpStatus.CREATED);
    }

    @PutMapping("/{id}/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<Player> updatePlayer(@PathVariable Integer id, @RequestBody Player question) {
        MDC.put("idAppel", idAppelTable + "2" + id);
        logger.info("Updating question with ID {}: {}", id, question);
        Player updatedPlayer = questionService.updatePlayer(id, question);
        return new ResponseEntity<>(updatedPlayer, HttpStatus.OK);
    }

    @DeleteMapping("/{id}/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<Void> deletePlayer(@PathVariable Integer id) {
        MDC.put("idAppel", idAppelTable + "3" + id);
        logger.info("Deleting question with ID: {}", id);
        questionService.deletePlayer(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}