/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Sae301.ApiMathsEduc.question;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

/**
 *
 * @author Utilisateur
 */
@RestController
@RequestMapping("/api/questions")
public class QuestionController {
    private final QuestionService questionService;
    Logger logger = LoggerFactory.getLogger(QuestionController.class); 
    String idAppelTable = "02"; // Table Questions

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping
    public ResponseEntity<List<EntityModel<Question>>> getAllQuestions() {
        MDC.put("idAppel", idAppelTable + "0");
        logger.info("Getting all questions");

        List<Question> questions = questionService.getAllQuestions();
        List<EntityModel<Question>> questionModels = new ArrayList<>();
        for (Question question : questions) {
            EntityModel<Question> model = EntityModel.of(question);
            Link selfLink = linkTo(methodOn(QuestionController.class).getQuestionById(question.getId())).withSelfRel();
            model.add(selfLink);
            questionModels.add(model);
        }
        return ResponseEntity.ok(questionModels);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<Question>> getQuestionById(@PathVariable Integer id) {
        MDC.put("idAppel", idAppelTable + "0" + id);
        logger.info("Getting question by ID: {}", id);

        Optional<Question> optionalQuestions = questionService.getQuestionById(id);
        if (optionalQuestions.isPresent()) {
            Question question = optionalQuestions.get();

            EntityModel<Question> model = EntityModel.of(question);

            Link selfLink = linkTo(methodOn(QuestionController.class).getQuestionById(id)).withSelfRel();
            model.add(selfLink);

            Link allQuestionsLink = linkTo(methodOn(QuestionController.class).getAllQuestions()).withRel("allQuestions");
            model.add(allQuestionsLink);

            return ResponseEntity.ok(model);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<Question> addQuestion(@RequestBody Question question) {
        MDC.put("idAppel", idAppelTable + "1");
        logger.info("Adding question: {}", question);
        Question addedQuestion = questionService.addQuestion(question);
        return new ResponseEntity<>(addedQuestion, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Question> updateQuestion(@PathVariable Integer id, @RequestBody Question question) {
        MDC.put("idAppel", idAppelTable + "2" + id);
        logger.info("Updating question with ID {}: {}", id, question);
        Question updatedQuestion = questionService.updateQuestion(id, question);
        return new ResponseEntity<>(updatedQuestion, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteQuestion(@PathVariable Integer id) {
        MDC.put("idAppel", idAppelTable + "3" + id);
        logger.info("Deleting question with ID: {}", id);
        questionService.deleteQuestion(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}