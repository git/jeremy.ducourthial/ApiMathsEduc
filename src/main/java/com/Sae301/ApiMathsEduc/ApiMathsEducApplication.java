package com.Sae301.ApiMathsEduc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMathsEducApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiMathsEducApplication.class, args);
	}

}
