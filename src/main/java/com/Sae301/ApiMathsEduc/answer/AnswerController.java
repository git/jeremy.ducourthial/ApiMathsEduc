/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Sae301.ApiMathsEduc.answer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

/**
 *
 * @author Utilisateur
 */
@RestController
@RequestMapping("/api/answers")
public class AnswerController {
    private final AnswerService answerService;
    Logger logger = LoggerFactory.getLogger(AnswerController.class); 
    String idAppelTable = "03"; // Table Answers

    @Autowired
    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @GetMapping("/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<List<EntityModel<Answer>>> getAllAnswers() {
        MDC.put("idAppel", idAppelTable + "0");
        logger.info("Getting all answers");

        List<Answer> answers = answerService.getAllAnswers();
        List<EntityModel<Answer>> answerModels = new ArrayList<>();
        for (Answer answer : answers) {
            EntityModel<Answer> model = EntityModel.of(answer);
            Link selfLink = linkTo(methodOn(AnswerController.class).getAnswerById(answer.getId())).withSelfRel();
            model.add(selfLink);
            answerModels.add(model);
        }
        return ResponseEntity.ok(answerModels);
    }

    @GetMapping("/{id}//qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<EntityModel<Answer>> getAnswerById(@PathVariable Integer id) {
        MDC.put("idAppel", idAppelTable + "0" + id);
        logger.info("Getting answer by ID: {}", id);

        Optional<Answer> optionalAnswer = answerService.getAnswerById(id);
        if (optionalAnswer.isPresent()) {
            Answer answer = optionalAnswer.get();

            EntityModel<Answer> model = EntityModel.of(answer);

            // Ajouter un lien vers le chapitre lui-même
            Link selfLink = linkTo(methodOn(AnswerController.class).getAnswerById(id)).withSelfRel();
            model.add(selfLink);

            // Ajouter un lien vers la liste complète des chapitres
            Link allAnswersLink = linkTo(methodOn(AnswerController.class).getAllAnswers()).withRel("allAnswers");
            model.add(allAnswersLink);

            return ResponseEntity.ok(model);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<Answer> addAnswer(@RequestBody Answer answer) {
        MDC.put("idAppel", idAppelTable + "1");
        logger.info("Adding answer: {}", answer);
        Answer addedAnswer = answerService.addAnswer(answer);
        return new ResponseEntity<>(addedAnswer, HttpStatus.CREATED);
    }

    @PutMapping("/{id}/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<Answer> updateAnswer(@PathVariable Integer id, @RequestBody Answer answer) {
        MDC.put("idAppel", idAppelTable + "2" + id);
        logger.info("Updating answer with ID {}: {}", id, answer);
        Answer updatedAnswer = answerService.updateAnswer(id, answer);
        return new ResponseEntity<>(updatedAnswer, HttpStatus.OK);
    }

    @DeleteMapping("/{id}/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
    public ResponseEntity<Void> deleteAnswer(@PathVariable Integer id) {
        MDC.put("idAppel", idAppelTable + "3" + id);
        logger.info("Deleting answer with ID: {}", id);
        answerService.deleteAnswer(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}