/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Sae301.ApiMathsEduc;

import org.hibernate.service.spi.ServiceException;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.ErrorResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * @author Utilisateur
 */
/*
@ControllerAdvice
public class ApiControllerAdvice extends ResponseEntityExceptionHandler{/
    @ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
    
    protected ResponseEntity<Object> handleConflict( RuntimeException ex, WebRequest request) {
        //Logger logger = LoggerFactory.getLogger(ApiControllerAdvice.class); 
        //logger.info(request.toString());

        String bodyOfResponse = "This should be application specific";
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request); 
    } 
}*/

@ControllerAdvice
public class ApiControllerAdvice {

    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<String> handleINTERNAL_SERVER_ERROR(ServiceException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<String> handleNOT_FOUND(NotFoundException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }
}